# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | /         |


---

### How to use 

    
    Quick go-through to all icons:
    
* ![](https://i.imgur.com/aBUhmJB.png) is brush tool. Pick it up and drag on the canvas to draw unregular lines and shapes. 
* ![](https://i.imgur.com/xq4YdUb.png), ![](https://i.imgur.com/JySAgNT.png), ![](https://i.imgur.com/2XbUV6n.png)are retangle tool, circle tool and triangle tool, respectively. These shapes come in as Stroke-only graphic by default. You can change its style by clicking solid tool ![](https://i.imgur.com/4Zil0Oy.png) and change the style back by clicking again.
* ![](https://i.imgur.com/BM9jnEP.png) is line tool. Pick it up and drag on the canvas to draw straight lines.
* Brush Thickness represents the thickness of the stroke of the above tools. Select from the list to change the value of thickness.
* ![](https://i.imgur.com/PK2S6ex.png)is text tool. You can type in then press enter to add text on the canvas. You can also choose the font's size and family by selecting from the two lists beside the tool.
* Color tool can apply color onto all the tools above.
* ![](https://i.imgur.com/Pa0y7ep.png)is refresh tool. You can clear your canvas by clicking it.
* ![](https://i.imgur.com/yDzHCf2.png), ![](https://i.imgur.com/ZGpDmmp.png) are redo and undo, respectively. 
* ![](https://i.imgur.com/8ZSjvxY.png)is download tool. You can save the current image on the canvas.
* Choose File allows you to upload an imgae onto the canvas.

### Function description

    Decribe how to implement functions including bouns function and how to use it.
* Handling mouse events
When we drag mouse onto the canvas, we need to record the current position of the mouse if we need to draw shape`(1)`. Then, onto mouse moving, canvas will present shapes according to the user's selection`(curr)`. Finally, the mouse is released we need to remove the listening events and add one layer to the file (just like adding layers in PS!)`(2)`.
```
function mouseMove(evt) {
  if (curr == 'brush') {
    ctx.lineWidth = document.getElementById("thick").value;
    ctx.strokeStyle = color.value;
    var imgData = layers[layers.length - 1];
    ctx.putImageData(imgData, 0, 0);
    ctx.lineTo(evt.clientX, evt.clientY + 32);
    ctx.stroke();
  } 
  else if (curr == 'rectangle' || curr == 'circle' || curr == 'triangle' || curr == 'line') {
    draw(evt);
  } 
  else if(curr == 'eraser') {
    ctx.clearRect(evt.clientX, evt.clientY, document.getElementById("thick").value+2, document.getElementById("thick").value)+2;
  }

};

var engage = function (evt) {
  if (curr == 'brush') {
    ctx.beginPath();
    ctx.moveTo(evt.clientX, evt.clientY + 32);
  }
  else if (curr== 'text' || curr == 'rectangle' || curr == 'circle' || curr == 'triangle' || curr == 'line') {
    engage_x = evt.clientX;
    engage_y = evt.clientY; //(1)
  }
  else if(curr == 'eraser') {
    ctx.clearRect(evt.clientX, evt.clientY, document.getElementById("thick").value+2, document.getElementById("thick").value+2);
  }
  canvas.addEventListener('mousemove', mouseMove, false);
}

canvas.addEventListener('mousedown', engage);

canvas.addEventListener('mouseup', function () {
  var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
  layers.push(imgData); //(2)
  remove.splice(0, remove.length);
  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);
```
* Drawing
For rectangle, basically we need to determine where are the four points by getting the event's X, Y and the offsets`(1)`. For circle, we use the same set of data to calculate its radius by using Pythagorean theorem`(2)` so that we can generate arcs. For triangle, the same set of data provides the starting point and draw two lines among all three points. For line, it's similar to triangle's but draw one less line. After that by counting we can determine whether the shape needs to be fill-up or not.
```
//(1)
var now_x = evt.clientX - canvas.getBoundingClientRect().left;
  var now_y = evt.clientY - canvas.getBoundingClientRect().top;

...

if(curr=='rectangle') {
    ctx.rect(engage_x, engage_y, now_x - engage_x, now_y - engage_y);
  } else if(curr=='circle') {
    var radius = Math.sqrt(Math.pow((engage_x - now_x), 2) + Math.pow((engage_y - now_y), 2)); //(2)
    ctx.arc(engage_x, engage_y, radius, 0, 2 * Math.PI, false);
  } else if(curr=='triangle') {
    ctx.moveTo(engage_x + (now_x - engage_x) / 2, engage_y);
    ctx.lineTo(now_x, now_y);
    ctx.lineTo(engage_x, now_y);
    ctx.closePath();
  } else if(curr=='line') {
    ctx.moveTo(engage_x, engage_y);
    ctx.lineTo(now_x, now_y);
    ctx.stroke();
  } 

  if (isSolid%2!=0 && curr!='line') {
    ctx.fill();
  } else {
    ctx.stroke();
  }
}
```
* redo/undo
Kinda like a open book with two side and after each operation on the canvas we turn a page. For redo/undo, we turn the one page to the other side, which means one half of the pages will "pop" (`layers.pop();`)a page and the other half will gain `(remove.push(layers[layers.length - 1]);)`a page.
```
redo.onclick = function(){
  if (layers.length > 0) {
    remove.push(layers[layers.length - 1]);
    layers.pop();
    ctx.putImageData(layers[layers.length - 1], 0, 0);
  }
}

undo.onclick = function() {
  if (remove.length > 0) {
    layers.push(remove[remove.length - 1]);
    remove.pop();
    ctx.putImageData(layers[layers.length - 1], 0, 0);
  }
}
```
*  upload
Make use of fileReader() and draw the image on the canvas
```
upload.addEventListener('change', function(ev) {
   if(ev.target.files) {
      let file = ev.target.files[0];
      var reader  = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function (e) {
          var image = new Image();
          image.src = e.target.result;
          
          image.onload = function(ev) {
             ctx.drawImage(image,0,0,600,400);
             var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
             layers.push(imgData);
             remove.splice(0, remove.length); 
         }
      }
   }
});
```
* download
```
download.onclick = function () {
  var gen = document.createElement('a'), e;
  gen.href = canvas.toDataURL('image/png');
  document.body.appendChild(gen);
  gen.download = "image"
  gen.click();
}
```

* refresh
Delete all the remaining layers and `clearRect()` functioned as a white board cover on the canvas. Also, the default tool and font tool is re-initialized here.
```
refresh.onclick = function() {
  remove.splice(0, remove.length);
  layers.splice(1, layers.length);
  ctx.putImageData(layers[0], 0, 0);
  ctx.clearRect(0,0,600,400);
  curr = 'brush';
  keyHistory="";
}
```
* Text tool
We need to add an EventListner to catch there's keyboard event.
```
text.onclick = function () {
  curr = 'text';
  keyHistory="";
  document.addEventListener("keyup", keyUpHandler, true);
  ...
}
```

For handling the keyboard input, define letters and if the according key is pressed, we need to either add text or end the text.

```
function keyUpHandler(event){
    var letters="abcdefghijklmnopqrstuvwxyz";
    var key=event.keyCode;
    if(key>64 && key<91){
      var letter=letters.substring(key-64,key-65);
      addletter(letter);
    }
    else if(key==13) {
      keyHistory="";
    }
}
```

We can simply put the string together, then we can adjust the character of the fonts. Fianlly, put the string onto the canvas.
```
function addletter(letter){
    keyHistory+=letter;
    if(fontSize.value==12) {
      if(fontFam.value=="arial")
        ctx.font = "12px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "12px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "12px Georgia";
    }
    else if(fontSize.value==16) {
      ...  
    }

    ctx.fillStyle = color.value;

    console.log(fontSize.value);
    ctx.fillText(keyHistory,engage_x,engage_y);
    var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
    layers.push(imgData);
    remove.splice(0, remove.length);
    
}
```
   
### Gitlab page link

    https://melody11381138.gitlab.io/as_01_webcanvas1

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>