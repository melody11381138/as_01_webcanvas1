var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');
var layers = [];
var remove = [];
var engage_x;
var engage_y;
var isSolid = 0;

var brush = document.getElementById("brush");
var rec = document.getElementById("rectangle");
var cir = document.getElementById("circle");
var tri = document.getElementById("triangle");
var lin = document.getElementById("line");
var solid = document.getElementById("solid");
var eraser = document.getElementById("eraser");
var refresh = document.getElementById("refresh");
var redo = document.getElementById("redo");
var undo = document.getElementById("undo");
var color = document.getElementById("color");
var upload = document.getElementById("upload");
var download = document.getElementById("download");
var fontSize = document.getElementById("font-size");
var fontFam = document.getElementById("font-family");

var curr = 'brush';

var keyHistory=""; 

function mouseMove(evt) {
  if (curr == 'brush') {
    ctx.lineWidth = document.getElementById("thick").value;
    ctx.strokeStyle = color.value;
    var imgData = layers[layers.length - 1];
    ctx.putImageData(imgData, 0, 0);
    ctx.lineTo(evt.clientX, evt.clientY + 32);
    ctx.stroke();
  } 
  else if (curr == 'rectangle' || curr == 'circle' || curr == 'triangle' || curr == 'line') {
    draw(evt);
  } 
  else if(curr == 'eraser') {
    ctx.clearRect(evt.clientX, evt.clientY, document.getElementById("thick").value+2, document.getElementById("thick").value)+2;
  }

};

var engage = function (evt) {
  if (curr == 'brush') {
    ctx.beginPath();
    ctx.moveTo(evt.clientX, evt.clientY + 32);
  }
  else if (curr== 'text' || curr == 'rectangle' || curr == 'circle' || curr == 'triangle' || curr == 'line') {
    engage_x = evt.clientX;
    engage_y = evt.clientY;
  }
  else if(curr == 'eraser') {
    ctx.clearRect(evt.clientX, evt.clientY, document.getElementById("thick").value+2, document.getElementById("thick").value+2);
  }
  canvas.addEventListener('mousemove', mouseMove, false);
}

canvas.addEventListener('mousedown', engage);

canvas.addEventListener('mouseup', function () {
  var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
  layers.push(imgData);
  remove.splice(0, remove.length);
  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

brush.onclick = function () {
  curr = 'brush';
  document.body.style.cursor = "url('icon/bursh.png'), auto";
}

rec.onclick = function () {
  curr = 'rectangle';
  document.body.style.cursor = "url('icon/rec.png'), auto";
}

cir.onclick = function () {
  curr = 'circle';
  document.body.style.cursor = "url('icon/cir.png'), auto";
}

tri.onclick = function () {
  curr = 'triangle';
  document.body.style.cursor = "url('icon/tri.png'), auto";
}

lin.onclick = function () {
  curr = 'line';
  document.body.style.cursor = "url('icon/line.png'), auto";
}

solid.onclick = function() {
  isSolid = isSolid + 1;
}

eraser.onclick = function () {
  curr = 'eraser';
  document.body.style.cursor = "url('image/eraser.png'), auto";
}

refresh.onclick = function() {
  remove.splice(0, remove.length);
  layers.splice(1, layers.length);
  ctx.putImageData(layers[0], 0, 0);
  ctx.clearRect(0,0,600,400);
  curr = 'brush';
  keyHistory="";
}

redo.onclick = function(){
  if (layers.length > 0) {
    remove.push(layers[layers.length - 1]);
    layers.pop();
    ctx.putImageData(layers[layers.length - 1], 0, 0);
  }
}

undo.onclick = function() {
  if (remove.length > 0) {
    layers.push(remove[remove.length - 1]);
    remove.pop();
    ctx.putImageData(layers[layers.length - 1], 0, 0);
  }
}

upload.addEventListener('change', function(ev) {
   if(ev.target.files) {
      let file = ev.target.files[0];
      var reader  = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function (e) {
          var image = new Image();
          image.src = e.target.result;
          
          image.onload = function(ev) {
             //var canvas = document.getElementById('canvas');
             //canvas.width = this.width;
             //canvas.height = this.height;
             //var ctx = canvas.getContext('2d');
             ctx.drawImage(image,0,0,600,400);
             var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
             layers.push(imgData);
             remove.splice(0, remove.length); 
         }
      }
   }
});

download.onclick = function () {
  var gen = document.createElement('a'), e;
  gen.href = canvas.toDataURL('image/png');
  document.body.appendChild(gen);
  gen.download = "image"
  gen.click();
}

function draw (evt) {
  var now_x = evt.clientX - canvas.getBoundingClientRect().left;
  var now_y = evt.clientY - canvas.getBoundingClientRect().top;
  var imgData = layers[layers.length - 1];
  ctx.putImageData(imgData, 0, 0);
  ctx.beginPath();
  ctx.lineWidth = document.getElementById("thick").value;
  ctx.strokeStyle = color.value;
  ctx.fillStyle = color.value;

  if(curr=='rectangle') {
    ctx.rect(engage_x, engage_y, now_x - engage_x, now_y - engage_y);
  } else if(curr=='circle') {
    var radius = Math.sqrt(Math.pow((engage_x - now_x), 2) + Math.pow((engage_y - now_y), 2));
    ctx.arc(engage_x, engage_y, radius, 0, 2 * Math.PI, false);
  } else if(curr=='triangle') {
    ctx.moveTo(engage_x + (now_x - engage_x) / 2, engage_y);
    ctx.lineTo(now_x, now_y);
    ctx.lineTo(engage_x, now_y);
    ctx.closePath();
  } else if(curr=='line') {
    ctx.moveTo(engage_x, engage_y);
    ctx.lineTo(now_x, now_y);
    ctx.stroke();
  } 

  if (isSolid%2!=0 && curr!='line') {
    ctx.fill();
  } else {
    ctx.stroke();
  }
}

text.onclick = function () {
  curr = 'text';
  keyHistory="";
  document.addEventListener("keyup", keyUpHandler, true);
  var imgData = layers[layers.length - 1];
  ctx.putImageData(imgData, 0, 0);
  document.body.style.cursor = "url('icon/text.png'), auto";
}

function addletter(letter){
    keyHistory+=letter;
    if(fontSize.value==12) {
      if(fontFam.value=="arial")
        ctx.font = "12px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "12px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "12px Georgia";
    }
    else if(fontSize.value==16) {
      if(fontFam.value=="arial")
        ctx.font = "16px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "16px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "16px Georgia";
    }
    else if(fontSize.value==20) {
      if(fontFam.value=="arial")
        ctx.font = "20px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "20px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "20px Georgia";      
    }
    else if(fontSize.value==22) {
      if(fontFam.value=="arial")
        ctx.font = "22px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "22px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "22px Georgia";      
    }
    else if(fontSize.value==24) {
      if(fontFam.value=="arial")
        ctx.font = "24px Arial";
      else if(fontFam.value=="impact")
        ctx.font = "24px Impact";
      else if(fontFam.value=="georgia")
        ctx.font = "24px Georgia";      
    }

    ctx.fillStyle = color.value;

    console.log(fontSize.value);
    ctx.fillText(keyHistory,engage_x,engage_y);
    var imgData = ctx.getImageData(0, 0, canvas.width+100, canvas.height+100);
    layers.push(imgData);
    remove.splice(0, remove.length);
    
}

function keyUpHandler(event){
    var letters="abcdefghijklmnopqrstuvwxyz";
    var key=event.keyCode;
    if(key>64 && key<91){
      var letter=letters.substring(key-64,key-65);
      addletter(letter);
    }
    else if(key==13) {
      keyHistory="";
    }
}